package com.lastparty;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.lastparty.config.Assets;
import com.lastparty.config.Input;
import com.lastparty.config.Paths;
import com.lastparty.config.Player;
import com.lastparty.scenes.Game;
import com.lastparty.scenes.MainMenu;

public class LastParty extends ApplicationAdapter
{
    public SpriteBatch batch;
    public Paths paths;
    public Assets assets;
    public Input input;
    public Player player;

    public Screen screens[];
    public int currentScreen;

    @Override
    public void create()
    {
        batch = new SpriteBatch();
        paths = new Paths();
        assets = new Assets(paths);
        input = new Input();
        player = new Player(new Vector2(38, 27), 3);

        screens = new Screen[]{new MainMenu(this), new Game(this)};
        currentScreen = 0;
    }

    @Override
    public void render()
    {
        screens[currentScreen].render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }
}
