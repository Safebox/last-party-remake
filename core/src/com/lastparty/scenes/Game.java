package com.lastparty.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.lastparty.LastParty;
import com.lastparty.config.States;

public class Game implements Screen
{
    private LastParty root;

    public Game(LastParty root)
    {
        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(root.input.ButtonR))
        {
            root.player.IsWeaponEquipped = !root.player.IsWeaponEquipped;
        }
        if (Gdx.input.isKeyPressed(root.input.ButtonL))
        {
            if (Gdx.input.isKeyJustPressed(root.input.ButtonDOWN))
            {
                if (root.player.SelectedAbility == States.EquippedAbility.Power)
                {
                    root.player.SelectedAbility = States.EquippedAbility.Weapon;
                }
            }
            else if (Gdx.input.isKeyJustPressed(root.input.ButtonLEFT))
            {
                if (root.player.SelectedWeapon != States.Weapons.Heart)
                {
                    root.player.SelectedWeapon = States.Weapons.getAll()[MathUtils.clamp(root.player.SelectedWeapon.get() - 1, 0,2)];
                }
                if (root.player.SelectedPower != States.ActivePowers.Blink)
                {
                    root.player.SelectedPower = States.ActivePowers.getAll()[MathUtils.clamp(root.player.SelectedPower.get() - 1, 0, 2)];
                }
            }
            else if (Gdx.input.isKeyJustPressed(root.input.ButtonRIGHT))
            {
                if (root.player.SelectedWeapon != States.Weapons.Crossbow_Sleep)
                {
                    root.player.SelectedWeapon = States.Weapons.getAll()[MathUtils.clamp(root.player.SelectedWeapon.get() + 1, 0,2)];
                }
                if (root.player.SelectedPower != States.ActivePowers.DevouringSwarm)
                {
                    root.player.SelectedPower = States.ActivePowers.getAll()[MathUtils.clamp(root.player.SelectedPower.get() + 1, 0, 2)];
                }
            }
            else if (Gdx.input.isKeyJustPressed(root.input.ButtonUP))
            {
                if (root.player.SelectedAbility == States.EquippedAbility.Weapon)
                {
                    root.player.SelectedAbility = States.EquippedAbility.Power;
                }
            }
        }
        else
        {
            if (root.player.velocity.equals(new Vector2()))
            {
                if (Gdx.input.isKeyPressed(root.input.ButtonDOWN))
                {
                    root.player.velocity = new Vector2(0, 1);
                    root.player.rotation = 0;
                }
                else if (Gdx.input.isKeyPressed(root.input.ButtonLEFT))
                {
                    root.player.velocity = new Vector2(-1, 0);
                    root.player.rotation = 1;
                }
                else if (Gdx.input.isKeyPressed(root.input.ButtonRIGHT))
                {
                    root.player.velocity = new Vector2(1, 0);
                    root.player.rotation = 2;
                }
                else if (Gdx.input.isKeyPressed(root.input.ButtonUP))
                {
                    root.player.velocity = new Vector2(0, -1);
                    root.player.rotation = 3;
                }
            }
            if (!root.player.velocity.equals(new Vector2()))
            {
                    root.player.Process(1/8f, (TiledMapTileLayer) root.assets.map.getLayers().get("Blocking"));
                            }

            if (root.player.manaTime < 1)
            {
                root.player.manaTime += (1f / (60f * root.player.ActivePowerLevels.get(root.player.lastPower)));
            }
            else if (root.player.manaTime > 1)
            {
                root.player.ResetManaTime();
            }

            if (root.player.weaponTime < 1)
            {
                root.player.weaponTime += (1f / 180f);
            }
            else if (root.player.weaponTime > 1)
            {
                root.player.ResetWeaponTime();
            }

            if (root.player.manaTime == 1f && root.player.weaponTime == 1f)
            {
                /*foreach (Entity o in enemyWorld.EntityManager.ActiveEntities)
                {
                    if (o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled || o.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                    {
                        gameStates.GameplayState = GameStates.PlayStates.EndGame;
                    }
                }
                foreach (Entity o in npcWorld.EntityManager.ActiveEntities)
                {
                    if (o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled || o.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                    {
                        gameStates.GameplayState = GameStates.PlayStates.EndGame;
                    }
                }*/
            }
        }
        if (root.player.IsWeaponEquipped)
        {
            if (Gdx.input.isKeyJustPressed(root.input.ButtonB))
            {
                switch (root.player.SelectedAbility)
                {
                    case Power:
                        if (root.player.manaTime == 1f)
                        {
                            switch (root.player.SelectedPower)
                            {
                                case Blink:
                                    root.player.ProcessBlink((TiledMapTileLayer)root.assets.map.getLayers().get("Blocking"));
                                    break;
                                case DarkVision:
                                    root.player.StartManaTime(States.ActivePowers.DarkVision);
                                    break;
                                case DevouringSwarm:
                                    //SystemPower.ProcessRats(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    //SystemPower.ProcessRats(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    break;
                            }
                        }
                        break;
                    case Weapon:
                        if (root.player.weaponTime == 1f)
                        {
                            switch (root.player.SelectedWeapon)
                            {
                                case Heart:
                                    //SystemWeapon.ProcessHeart(e, npcWorld.EntityManager.ActiveEntities, gameStates, assets, gameStates.Target);
                                    break;
                                case Crossbow_Attack:
                                    //SystemWeapon.ProcessArrow(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    //SystemWeapon.ProcessArrow(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    break;
                                case Crossbow_Sleep:
                                    //SystemWeapon.ProcessSleepArrow(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    //SystemWeapon.ProcessSleepArrow(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                    break;
                            }
                        }
                        break;
                }
            }
            else if (Gdx.input.isKeyJustPressed(root.input.ButtonA))
            {
                Vector2 direction = new Vector2();
                switch (root.player.rotation)
                {
                    case 0:
                        direction = new Vector2(0, 1);
                        break;
                    case 1:
                        direction = new Vector2(-1, 0);
                        break;
                    case 2:
                        direction = new Vector2(1, 0);
                        break;
                    case 3:
                        direction = new Vector2(0, -1);
                        break;
                }
                /*foreach (Entity o in enemyWorld.EntityManager.ActiveEntities)
                {
                    if (o.GetComponent<ArtemisComponents.Transform>().Position == e.GetComponent<ArtemisComponents.Transform>().Position + direction)
                    {
                        o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                    }
                }
                foreach (Entity o in npcWorld.EntityManager.ActiveEntities)
                {
                    if (o.GetComponent<ArtemisComponents.Transform>().Position == e.GetComponent<ArtemisComponents.Transform>().Position + direction)
                    {
                        o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                    }
                }*/
            }
        }

        //Render
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        root.batch.begin();
        //Map - 1 : 5
        for (int l = 0; l < 4; l++)
        {
            TiledMapTileLayer mtl = (TiledMapTileLayer) root.assets.map.getLayers().get(l);
            for (int x = 0; x < mtl.getWidth(); x++)
            {
                for (int y = 0; y < mtl.getHeight(); y++)
                {
                    if (mtl.getCell(x, y) == null)
                    {
                        continue;
                    }
                    TiledMapTile t = mtl.getCell(x, y).getTile();
                    Vector2 posOffset = new Vector2(x * 8 + 240f / 2f - root.player.PositionNormal().x - 4, (y - mtl.getHeight()) * 8 + 160 / 2f + root.player.PositionNormal().y + 4);
                    Vector2 texOffset = new Vector2((int) ((t.getId() - 1) % 16f) * 8, (int) ((t.getId() - 1) / 16f) * 8);
                    root.batch.draw(root.assets.Tileset, posOffset.x, posOffset.y, (int) texOffset.x, (int) texOffset.y, 8, 8);
                }
            }
        }
        //Characters
        //Guests behind Player
        for (MapObject o : root.assets.map.getLayers().get(7).getObjects())
        {
            Vector2 pos = new Vector2((float) Double.parseDouble(o.getProperties().get("x").toString()), (float) Double.parseDouble(o.getProperties().get("y").toString()));
            Vector2 posOffset = new Vector2((int) ((pos.x / 16f) * 8) + 240f / 2f - root.player.PositionNormal().x - 4, (int) ((pos.y / 16f - 256) * 8) + 160 / 2f + root.player.PositionNormal().y + 4);
            if (MathUtils.round((256 - pos.y / 16f) * 8) >= root.player.PositionNormal().y + 7 + 16)
            {
                continue;
            }
            if (o.getProperties().get("type").equals("1"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("2"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("3"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }

            if (o.getProperties().get("type").equals("T1"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("T2"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("T3"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
        }
        //Enemies behind Player
        for (MapObject o : root.assets.map.getLayers().get(6).getObjects())
        {
            Vector2 pos = new Vector2((float) Double.parseDouble(o.getProperties().get("x").toString()), (float) Double.parseDouble(o.getProperties().get("y").toString()));
            Vector2 posOffset = new Vector2((int) ((pos.x / 16f) * 8) + 240f / 2f - root.player.PositionNormal().x - 4, (int) ((pos.y / 16f - 256) * 8) + 160 / 2f + root.player.PositionNormal().y + 4);
            if (MathUtils.round((256 - pos.y / 16f) * 8) >= root.player.PositionNormal().y + 7 + 16)
            {
                continue;
            }
            if (o.getProperties().get("type").equals("G"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("M"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
        }
        //Player
        root.batch.setColor(Color.BLACK);
        root.batch.draw(root.assets.PlayerSprite, (int) (240 / 2f - 4) + 1, (int) (160 / 2f - 14f), 32  + (8 * root.player.AnimFrame()), 16 * root.player.rotation, 8, 16);
        root.batch.draw(root.assets.PlayerSprite, (int) (240 / 2f - 4) - 1, (int) (160 / 2f - 14f), 32  + (8 * root.player.AnimFrame()), 16 * root.player.rotation, 8, 16);
        root.batch.draw(root.assets.PlayerSprite, (int) (240 / 2f - 4), (int) (160 / 2f - 14f) + 1, 32  + (8 * root.player.AnimFrame()), 16 * root.player.rotation, 8, 16);
        root.batch.draw(root.assets.PlayerSprite, (int) (240 / 2f - 4), (int) (160 / 2f - 14f) - 1, 32  + (8 * root.player.AnimFrame()), 16 * root.player.rotation, 8, 16);

        root.batch.setColor(Color.WHITE);
        root.batch.draw(root.assets.PlayerSprite, (int) (240 / 2f - 4), (int) (160 / 2f - 14f), 32  + (8 * root.player.AnimFrame()), 16 * root.player.rotation, 8, 16);
        //Guests in front of Player
        for (MapObject o : root.assets.map.getLayers().get(7).getObjects())
        {
            Vector2 pos = new Vector2((float) Double.parseDouble(o.getProperties().get("x").toString()), (float) Double.parseDouble(o.getProperties().get("y").toString()));
            Vector2 posOffset = new Vector2((int) ((pos.x / 16f) * 8) + 240f / 2f - root.player.PositionNormal().x - 4, (int) ((pos.y / 16f - 256) * 8) + 160 / 2f + root.player.PositionNormal().y + 4);
            if (MathUtils.round((256 - pos.y / 16f) * 8) < root.player.PositionNormal().y + 7 + 16)
            {
                continue;
            }
            if (o.getProperties().get("type").equals("1"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest1Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("2"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest2Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("3"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Guest3Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }

            if (o.getProperties().get("type").equals("T1"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target1Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("T2"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target2Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("T3"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.Target3Sprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
        }
        //Enemies in front of Player
        for (MapObject o : root.assets.map.getLayers().get(6).getObjects())
        {
            Vector2 pos = new Vector2((float) Double.parseDouble(o.getProperties().get("x").toString()), (float) Double.parseDouble(o.getProperties().get("y").toString()));
            Vector2 posOffset = new Vector2((int) ((pos.x / 16f) * 8) + 240f / 2f - root.player.PositionNormal().x - 4, (int) ((pos.y / 16f - 256) * 8) + 160 / 2f + root.player.PositionNormal().y + 4);
            if (MathUtils.round((256 - pos.y / 16f) * 8) < root.player.PositionNormal().y + 7 + 16)
            {
                continue;
            }
            if (o.getProperties().get("type").equals("G"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.GuardSprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
            if (o.getProperties().get("type").equals("M"))
            {
                root.batch.setColor(Color.BLACK);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x + 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x - 1, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y + 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y - 1, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);

                root.batch.setColor(Color.WHITE);
                root.batch.draw(root.assets.MusicGuardSprite, (int) posOffset.x, (int) posOffset.y, 8, 16 * Integer.valueOf(o.getProperties().get("Rot").toString()), 8, 16);
            }
        }
        //Map - 6 : 7
        for (int l = 4; l < 6; l++)
        {
            TiledMapTileLayer mtl = (TiledMapTileLayer) root.assets.map.getLayers().get(l);
            for (int x = 0; x < mtl.getWidth(); x++)
            {
                for (int y = 0; y < mtl.getHeight(); y++)
                {
                    if (mtl.getCell(x, y) == null)
                    {
                        continue;
                    }
                    TiledMapTile t = mtl.getCell(x, y).getTile();
                    Vector2 posOffset = new Vector2(x * 8 + 240f / 2f - root.player.PositionNormal().x - 4, (y - mtl.getHeight()) * 8 + 160 / 2f + root.player.PositionNormal().y + 4);
                    Vector2 texOffset = new Vector2((int) ((t.getId() - 1) % 16f) * 8, (int) ((t.getId() - 1) / 16f) * 8);
                    root.batch.draw(root.assets.Tileset, posOffset.x, posOffset.y, (int) texOffset.x, (int) texOffset.y, 8, 8);
                }
            }
        }
        //HUD
        root.batch.draw(root.assets.HUD, 0, 0);
        root.batch.draw(root.assets.Bars, 4, 44, 0, 0, root.assets.Bars.getWidth() / 2, root.assets.Bars.getHeight());
        root.batch.draw(root.assets.Bars, 240 - 12, 44, 8, 0, 8, (int)(root.assets.Bars.getHeight() * root.player.manaTime));
        if (root.player.IsWeaponEquipped)
        {
            root.batch.draw(root.assets.SwordIcon, 240 - 24, + 24);
            switch (root.player.SelectedAbility)
            {
                case Weapon:
                    root.batch.draw(root.assets.Icons, 240 - 40, 8, 16 * root.player.SelectedWeapon.get(), 16, 16, 16);
                    break;
                case Power:
                    root.batch.draw(root.assets.Icons, 240 - 40, 8, 16 * root.player.SelectedPower.get(), 0, 16, 16);
                    break;
            }
        }
        else
        {
            root.batch.draw(root.assets.Icons, 240 - 24, + 24, root.assets.Icons.getWidth() - 16, 16, 16, 16);
            root.batch.draw(root.assets.Icons, 240 - 40, 8, root.assets.Icons.getWidth() - 16, 16, 16, 16);
        }
        //Equipment
        if (Gdx.input.isKeyPressed(root.input.ButtonL))
        {
            root.batch.draw(root.assets.EquipmentHUD, 0, 0);
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if (y != 0)
                    {
                        root.batch.draw(root.assets.Icons, (240 / 2f) + x * 64 - 8, (5.5f * 16) - y * 20, 16 + (16 * x), 8 + 8 * y, 16, 16);
                    }
                }
            }
            switch (root.player.SelectedAbility)
            {
                case Weapon:
                    root.batch.draw(root.assets.Selection, (240 / 2f) + (root.player.SelectedWeapon.get() - 1) * 64 - 16, (5.5f * 16) - 20 - 8);
                    root.assets.MenuFont.draw(root.batch, root.assets.WeaponNames[root.player.SelectedWeapon.get()],240 / 2f - root.assets.WeaponNames[root.player.SelectedWeapon.get()].width / 2f, 32);
                    break;
                case Power:
                    root.batch.draw(root.assets.Selection, (240 / 2f) + (root.player.SelectedPower.get() - 1) * 64 - 16, (5.5f * 16) + 20 - 8);
                    root.assets.MenuFont.draw(root.batch, root.assets.PowerNames[root.player.SelectedPower.get()],240 / 2f - root.assets.PowerNames[root.player.SelectedPower.get()].width / 2f, 32);
                    break;
            }
        }
        root.batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
