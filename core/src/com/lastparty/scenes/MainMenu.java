package com.lastparty.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.MathUtils;
import com.lastparty.LastParty;

public class MainMenu implements Screen
{
    private int curSelection;
    private String[] menuText;
    private GlyphLayout[] menuItems;
    private Pixmap selectionBar;

    private LastParty root;

    public MainMenu(LastParty root)
    {
        curSelection = 0;
        menuItems = new GlyphLayout[] {new GlyphLayout(), new GlyphLayout(), new GlyphLayout()};
        menuText = new String[]{"New Game","Load Game","Options"};
        for (int m = 0; m < menuItems.length; m++)
        {
            menuItems[m].setText(root.assets.MenuFont, menuText[m]);
        }
        selectionBar = new Pixmap(240,28, Pixmap.Format.RGBA8888);
        selectionBar.setColor(Color.WHITE);
        selectionBar.fill();

        this.root = root;
    }

    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        //Update
        if (Gdx.input.isKeyJustPressed(root.input.ButtonDOWN))
        {
            curSelection = MathUtils.clamp(curSelection + 1, 0, 2);
        }
        if (Gdx.input.isKeyJustPressed(root.input.ButtonUP))
        {
            curSelection = MathUtils.clamp(curSelection - 1, 0, 2);
        }
        for (int m = 0; m < menuItems.length; m++)
        {
            if (m == curSelection)
            {
                root.assets.MenuFont.setColor(Color.valueOf("181425"));
            }
            else
            {
                root.assets.MenuFont.setColor(Color.WHITE);
            }
            menuItems[m].setText(root.assets.MenuFont, menuText[m]);
        }
        if (Gdx.input.isKeyJustPressed(root.input.ButtonA))
        {
            switch (curSelection)
            {
                case 0:
                    root.currentScreen = 1;
                    break;
                case 1:
                    break;
                case 2:
                    //root.currentScreen = 2;
                    break;
            }
        }

        //Render
        Gdx.gl.glClearColor(24 / 255f, 20 / 255f, 37 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        root.batch.begin();
        //root.batch.draw(root.assets.Signature, (240 / 2) - (208 / 2), 160 - 16);
        root.batch.draw(root.assets.Copyright, (240 / 2) - (root.assets.Copyright.getWidth() / 2), 0);
        root.batch.draw(root.assets.Logo, (240 / 2) - (208 / 2), 160 - (int)(root.assets.Logo.getHeight() * (208f / 1024f)) - 16, 208, (int)(root.assets.Logo.getHeight() * (208f / 1024f)));
        root.batch.draw(new Texture(selectionBar), 0, 32 + 24 - (24 * curSelection));
        root.assets.MenuFont.draw(root.batch, menuItems[0], 16, 32 + 48);
        root.assets.MenuFont.draw(root.batch, menuItems[1], 240 / 2 - menuItems[1].width / 2, 32 + 24);
        root.assets.MenuFont.draw(root.batch, menuItems[2], 240 - menuItems[2].width - 16, 32);
        root.batch.end();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
