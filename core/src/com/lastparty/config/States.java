package com.lastparty.config;

import com.badlogic.gdx.math.MathUtils;

public class States
{
    public enum EquippedAbility
    {
        Weapon(0), Power(1);
        private int value;

        EquippedAbility(int v)
        {
            value = v;
        }

        public int get()
        {
            return value;
        }
    }

    public enum Weapons
    {
        Heart(0), Crossbow_Attack(1), Crossbow_Sleep(2), Grenade(3), Pistol(4), Springrazor(5);
        private int value;

        Weapons(int v)
        {
            value = v;
        }

        public int get()
        {
            return value;
        }

        public static Weapons[] getAll()
        {
            return new Weapons[]{Heart, Crossbow_Attack, Crossbow_Sleep};
        }
    }

    public enum ActivePowers
    {
        Blink(0), DarkVision(1), DevouringSwarm(2), Possession(3), BendTime(4), WindBlast(5);
        private int value;

        ActivePowers(int v)
        {
            value = v;
        }

        public int get()
        {
            return value;
        }

        public static ActivePowers[] getAll()
        {
            return new ActivePowers[]{Blink, DarkVision, DevouringSwarm};
        }
    }

    public enum PassivePowers
    {
        Vitality(0), BloodThirsty(1), Agility(2), ShadowKill(3);
        private int value;

        PassivePowers(int v)
        {
            value = v;
        }

        public int get()
        {
            return value;
        }
    }
}
