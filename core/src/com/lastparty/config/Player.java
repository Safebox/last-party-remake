package com.lastparty.config;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;

public class Player
{
    //Transform
    public Vector2 lastPosition;
    public Vector2 position;
    public int rotation;
    public Vector2 velocity;

    public Vector2 PositionNormal()
    {
        return new Vector2(position.x * 8f, (position.y - 2) * 8f - 3);
    }

    public void Process(float time, TiledMapTileLayer collisionMap)
    {
        Vector2 newPos = new Vector2(lastPosition.x + velocity.x, lastPosition.y + velocity.y);

        if (collisionMap.getCell((int) newPos.x, 256 - (int) newPos.y) != null)
        {
            if (!position.equals(newPos))
            {
                //float adjustedTime = (float) (Math.sin(animTime * Math.PI - Math.PI / 2) + 1) / 2f;
                position = new Vector2(MathUtils.lerp(lastPosition.x, newPos.x, animTime), MathUtils.lerp(lastPosition.y, newPos.y, animTime));
                animTime += time;
                if (animTime > 1f)
                {
                    position = newPos;
                }
            }
            else
            {
                position = new Vector2((int) position.x, (int) position.y);
                lastPosition = position;
                ResetAnimTime();
                velocity = new Vector2();
            }
        }
        else
        {
            velocity = new Vector2();
        }
    }

    //Equipment
    public boolean IsWeaponEquipped;
    public States.EquippedAbility SelectedAbility;
    public States.Weapons SelectedWeapon;
    public States.ActivePowers SelectedPower;
    public HashMap<States.ActivePowers, Byte> ActivePowerLevels;
    public HashMap<States.PassivePowers, Byte> PassivePowerLevels;
    public float manaTime;
    public States.ActivePowers lastPower;
    public States.ActivePowers activePower;
    public float weaponTime;
    public States.Weapons activeWeapon;

    public void ResetManaTime()
    {
        manaTime = 1;
        activePower = null;
    }

    public void StartManaTime(States.ActivePowers power)
    {
        manaTime = 0;
        activePower = power;
    }

    public void ResetWeaponTime()
    {
        weaponTime = 1;
    }

    public void StartWeaponTime(States.Weapons weapon)
    {
        weaponTime = 0;
        activeWeapon = weapon;
    }

    public void ProcessBlink(TiledMapTileLayer blocking)
    {
        switch (rotation)
        {
            case 0:
                velocity = new Vector2(0, 1);
                break;
            case 1:
                velocity = new Vector2(-1, 0);
                break;
            case 2:
                velocity = new Vector2(1, 0);
                break;
            case 3:
                velocity = new Vector2(0, -1);
                break;
        }
        Vector2 offset = velocity;
        for (int mult = 1; mult < 5 * ActivePowerLevels.get(lastPower); mult++)
        {
            offset = new Vector2(velocity.x * mult, velocity.y * mult);
            if (blocking.getCell((int)(position.x + offset.x), (int)(position.x + offset.y)) != null)
            {
                offset = new Vector2(velocity.x * (mult - 1), velocity.y * (mult - 1));
                break;
            }
        }
        velocity = offset;
        lastPower = States.ActivePowers.Blink;
        StartManaTime(States.ActivePowers.Blink);
    }

    public void ProcessDarkVision(MapObjects others, SpriteBatch sb, Texture hintIcon, float strength)
    {
        for (MapObject e : others)
        {
            Vector2 pos = new Vector2((float)Double.parseDouble(e.getProperties().get("x").toString()) - position.x, (float)Double.parseDouble(e.getProperties().get("y").toString()) - position.y);
            pos = new Vector2((float)Math.sqrt(pos.x * pos.x + pos.y * pos.y) * pos.x, (float)Math.sqrt(pos.x * pos.x + pos.y * pos.y) * pos.y);
            if (!e.getName().contains("T"))
            {
                sb.setColor(new Color((1 - strength), (1 - strength), (1 - strength), (1 - strength)));
            }
            else if (e.getName().contains("T"))
            {
                sb.setColor(new Color(255 * (1 - strength), 0, 68 * (1 - strength), (1 - strength)));
            }
            sb.draw(hintIcon, 240 / 2f + pos.x * 3 * 8 - 8, 160 / 2f+ pos.x * 3 * 8 - 8);
            sb.setColor(Color.WHITE);
        }
        lastPower = States.ActivePowers.DarkVision;
    }

    //Remove or replace!
    /*public void ProcessRats(MapObjects others, SpriteBatch sb, Assets assets, float strength)
    {
        Vector2 direction = new Vector2();
        switch (rotation)
        {
            case 0:
                direction = new Vector2(0, 1);
                break;
            case 1:
                direction = new Vector2(-1, 0);
                break;
            case 2:
                direction = new Vector2(1, 0);
                break;
            case 3:
                direction = new Vector2(0, -1);
                break;
        }
        Vector2 offset = direction;
        for (int mult = 1; mult < 3 * entity.GetComponent < ArtemisComponents.Equipment > ().GetLastPowerLevel; mult++)
        {
            offset = direction * mult;
            if (blocking.Tiles[(int) ((entity.GetComponent < ArtemisComponents.Transform > ().Position.Y + offset.Y) * 256) + (int) (entity.GetComponent < ArtemisComponents.Transform > ().Position.X + offset.X)].Gid == 0)
            {
                offset = direction * (mult - 1);
                break;
            }
        }
        foreach(Entity e in others)
        {
            if (e.GetComponent < ArtemisComponents.Transform > ().Position == entity.GetComponent < ArtemisComponents.Transform > ().Position + direction)
            {
                e.GetComponent<ArtemisComponents.NPC> ().HasBeenKilled = true;
            }
        }
        entity.GetComponent<ArtemisComponents.Equipment> ().SetLastPower(GameStates.ActivePowers.DevouringSwarm);
        entity.GetComponent<ArtemisComponents.Equipment> ().StartTime(GameStates.ActivePowers.DevouringSwarm);
    }*/

    //Animation
    public float animTime;

    public int AnimFrame()
    {
        return (int) Math.round(Math.sin(2 * Math.PI * animTime) * 4f);
    }

    public void ResetAnimTime()
    {
        animTime = 0;
    }

    public Player(Vector2 pos, int rot)
    {
        //Transform
        lastPosition = pos;
        position = pos;
        rotation = rot;
        velocity = new Vector2();

        //Equipment
        IsWeaponEquipped = false;
        SelectedAbility = States.EquippedAbility.Power;
        SelectedWeapon = States.Weapons.Heart;
        SelectedPower = States.ActivePowers.Blink;
        ActivePowerLevels = new HashMap<States.ActivePowers, Byte>();
        ActivePowerLevels.put(States.ActivePowers.Blink, (byte) 1);
        ActivePowerLevels.put(States.ActivePowers.DarkVision, (byte) 1);
        ActivePowerLevels.put(States.ActivePowers.DevouringSwarm, (byte) 1);
        ActivePowerLevels.put(States.ActivePowers.Possession, (byte) 0);
        ActivePowerLevels.put(States.ActivePowers.BendTime, (byte) 0);
        ActivePowerLevels.put(States.ActivePowers.WindBlast, (byte) 0);
        PassivePowerLevels = new HashMap<States.PassivePowers, Byte>();
        PassivePowerLevels.put(States.PassivePowers.Vitality, (byte) 0);
        PassivePowerLevels.put(States.PassivePowers.BloodThirsty, (byte) 0);
        PassivePowerLevels.put(States.PassivePowers.Agility, (byte) 0);
        PassivePowerLevels.put(States.PassivePowers.ShadowKill, (byte) 0);
        manaTime = 1f;
        weaponTime = 1f;
        lastPower = States.ActivePowers.Blink;
    }
}
