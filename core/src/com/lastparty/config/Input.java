package com.lastparty.config;

import com.badlogic.gdx.Input.Keys;

public class Input
{
    public int ButtonA = Keys.L;
    public int ButtonB = Keys.K;
    public int ButtonSEL = Keys.SPACE;
    public int ButtonSTR = Keys.ESCAPE;
    public int ButtonRIGHT = Keys.D;
    public int ButtonLEFT = Keys.A;
    public int ButtonUP = Keys.W;
    public int ButtonDOWN = Keys.S;
    public int ButtonR = Keys.O;
    public int ButtonL = Keys.Q;
}
