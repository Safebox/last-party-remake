package com.lastparty.config;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class Assets
{
    //Fonts
    public BitmapFont MenuFont;
    public BitmapFont CommonFont;
    public String[] Target1Hints;
    public String[] Target2Hints;
    public String[] Target3Hints;
    public GlyphLayout[] PowerNames;
    public GlyphLayout[] WeaponNames;

    //Maps
    public TiledMap map;

    //Textures
    public Texture Signature;
    public Texture Copyright;
    public Texture Logo;
    public Texture Cursor;
    public Texture HUD;
    public Texture EquipmentHUD;
    public Texture Selection;
    public Texture Bars;
    public Texture Icons;
    public Texture SwordIcon;
    public Texture Tileset;

    //Sprites
    public Texture PlayerSprite;
    public Texture GuardSprite;
    public Texture MusicGuardSprite;
    public Texture Guest1Sprite;
    public Texture Guest2Sprite;
    public Texture Guest3Sprite;
    public Texture Target1Sprite;
    public Texture Target2Sprite;
    public Texture Target3Sprite;

    public Texture RatsSprite;

    //Sounds

    //Music
    public Music MenuTheme;
    public Music BackgroundMusic1;
    public Music BackgroundMusic2;

    public Assets(Paths paths)
    {
        MenuFont = new BitmapFont(Gdx.files.internal("Van-Helsing.fnt"));
        CommonFont = new BitmapFont(Gdx.files.internal("Impact.fnt"));
        Target1Hints = new String[]{
                "She is paranoid. And rightly so.",
                "She likes to stand out with vibrancy.",
                "Her 'lover' awaits nearby. He was not invited."};
        Target2Hints = new String[]{
                "She has bedded many men in this room.",
                "Alcohol has stained this carpet, more than once.",
                "It was here she met His Lordship."};
        Target3Hints = new String[]{
                "She has talent beyond compare.",
                "She prefers the company of her equals.",
                "You can hear her now, listen..."};
        PowerNames = new GlyphLayout[]{new GlyphLayout(), new GlyphLayout(), new GlyphLayout()};
        PowerNames[0].setText(MenuFont, "Warp");
        PowerNames[1].setText(MenuFont, "Sixth Sense");
        PowerNames[2].setText(MenuFont, "Infestation");
        WeaponNames = new GlyphLayout[]{new GlyphLayout(), new GlyphLayout(), new GlyphLayout()};
        WeaponNames[0].setText(MenuFont, "Hidden Insight");
        WeaponNames[1].setText(MenuFont, "Poison Arrow");
        WeaponNames[2].setText(MenuFont, "Sleep Arrow");

        map = new TmxMapLoader().load(paths.MapsPath + "GroundFloor.tmx");

        Signature = new Texture(paths.TexturesPath + "Sig.png");
        Copyright = new Texture(paths.TexturesPath + "Copyright.png");
        Logo = new Texture(paths.TexturesPath + "Logo.png");
        Cursor = new Texture(paths.TexturesPath + "Cursor.png");
        HUD = new Texture(paths.TexturesPath + "HUD.png");
        EquipmentHUD = new Texture(paths.TexturesPath + "Quickwheel.png");
        Selection = new Texture(paths.TexturesPath + "Selection.png");
        Bars = new Texture(paths.TexturesPath + "Bars.png");
        Icons = new Texture(paths.TexturesPath + "Equipment.png");
        SwordIcon = new Texture(paths.TexturesPath + "Sword.png");
        Tileset = new Texture(paths.TexturesPath + "Tileset.png");

        PlayerSprite = new Texture(paths.SpritesPath + "Player.png");

        GuardSprite = new Texture(paths.SpritesPath + "Guard.png");
        MusicGuardSprite = new Texture(paths.SpritesPath + "MusicGuard.png");
        Guest1Sprite = new Texture(paths.SpritesPath + "Guest1.png");
        Guest2Sprite = new Texture(paths.SpritesPath + "Guest2.png");
        Guest3Sprite = new Texture(paths.SpritesPath + "Guest3.png");
        Target1Sprite = new Texture(paths.SpritesPath + "Target1.png");
        Target2Sprite = new Texture(paths.SpritesPath + "Target2.png");
        Target3Sprite = new Texture(paths.SpritesPath + "Target3.png");

        RatsSprite = new Texture(paths.SpritesPath + "Rats.png");

        MenuTheme = Gdx.audio.newMusic(Gdx.files.internal(paths.MusicPath + "Kai_Engel_-_04_-_Moonlight_Reprise.mp3"));
        BackgroundMusic1 = Gdx.audio.newMusic(Gdx.files.internal(paths.MusicPath + "HWV 317 - Part 1.mp3"));
        BackgroundMusic2 = Gdx.audio.newMusic(Gdx.files.internal(paths.MusicPath + "HWV 317 - Part 2.mp3"));
    }
}
