package com.lastparty.config;

public class Paths
{
    public String MapsPath;
    public String SpritesPath;
    public String TexturesPath;
    public String SoundsPath;
    public String MusicPath;

    public Paths()
    {
        MapsPath = "Maps\\";
        SpritesPath = "Sprites\\";
        TexturesPath = "Textures\\";
        SoundsPath = "Sounds\\";
        MusicPath = "Music\\";
    }
}
